# Author : Bryan Haymoz
# Date   : 12.01.2022
# Version: 1.2
# Class  : T-3a
# Project: Semester Project 5 - Automated Deployement of VM's
# Purpose: To associate public key for SSH connection

data "external" "get_ssh_key" {
  program = ["bash", "../Bash/get_ssh_key.sh"]
}

resource "openstack_compute_keypair_v2" "vm-key" {
  name = "vm-key"
  public_key = "${data.external.get_ssh_key.result.SSHKEY}"
}