# Author : Bryan Haymoz
# Date   : 12.01.2022
# Version: 1.1
# Class  : T-3a
# Project: Semester Project 5 - Automated Deployement of VM's
# Purpose: To deploy router

#obtain external network
data "openstack_networking_network_v2" "external_network" {
  name = "public"
}

#Create router
resource "openstack_networking_router_v2" "router_service" {
  name = "router_service"
  external_network_id = data.openstack_networking_network_v2.external_network.id
  admin_state_up  = "true"
}

#Create interface to service network
resource "openstack_networking_port_v2" "int_router_service" {
  name = "int_router_service"
  network_id = "${openstack_networking_network_v2.service_network.id}"
  admin_state_up = "true"
  port_security_enabled = "true"
  security_group_ids = ["${openstack_compute_secgroup_v2.secgroup_1.id}"]
  fixed_ip {
    subnet_id = "${openstack_networking_subnet_v2.service_subnet.id}"
    ip_address = "${var.service_ip_gateway}"
  }
}

#Associate interface to router
resource "openstack_networking_router_interface_v2" "router_interface_1" {
  router_id = "${openstack_networking_router_v2.router_service.id}"
  port_id = "${openstack_networking_port_v2.int_router_service.id}"
}