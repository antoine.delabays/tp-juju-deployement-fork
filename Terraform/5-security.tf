# Author : Bryan Haymoz
# Date   : 08.12.2021
# Version: 1.0
# Class  : T-3a
# Project: Semester Project 5 - Automated Deployement of VM's
# Purpose: To manage security rules
# Comments : When creating new security group following security rules are add by default to allow communication between instance
# Rule 1 : Ingress - IPv6 - All protocols - All ports - CIDR 0.0.0.0/0
# Rule 1 : Ingress - IPv4 - All protocols - All ports - CIDR 0.0.0.0/0

resource "openstack_compute_secgroup_v2" "secgroup_1" {
  name        = "my_secgroup"
  description = "my security group"

  #Authorize SSH
  rule {
    from_port   = 22
    to_port     = 22
    ip_protocol = "tcp"
    cidr        = "0.0.0.0/0"
  }

  #Authorize connection to MAAS web interface
  rule {
    from_port   = 5240
    to_port     = 5240
    ip_protocol = "tcp"
    cidr        = "0.0.0.0/0"
  }

  #Authorize ICMP
  rule {
    from_port   = -1
    to_port     = -1
    ip_protocol = "icmp"
    cidr        = "0.0.0.0/0"
  }
}