# Author : Bryan Haymoz
# Date   : 20.12.2021
# Version: 1.0
# Class  : T-3a
# Project: Semester Project 5 - Automated Deployement of VM's
# Purpose: Template for inventory of Terraform for Ansible

${hostname_MAAS} ansible_ssh_user=${user} ansible_ssh_host=${IP_MAAS} ansible_ssh_port=${port_SSH}

${hostname_Juju} ansible_ssh_user=${user} ansible_ssh_host=${IP_Juju} ansible_ssh_port=${port_SSH}

${hostname_Node1} ansible_ssh_user=${user} ansible_ssh_host=${IP_Node1} ansible_ssh_port=${port_SSH}

${hostname_Node2} ansible_ssh_user=${user} ansible_ssh_host=${IP_Node2} ansible_ssh_port=${port_SSH}

${hostname_Node3} ansible_ssh_user=${user} ansible_ssh_host=${IP_Node3} ansible_ssh_port=${port_SSH}