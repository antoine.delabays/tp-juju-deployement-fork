# Author : Bryan Haymoz
# Date   : 02.01.2022
# Version: 1.0
# Class  : T-3a
# Project: Semester Project 5 - Automated Deployement of VM's
# Purpose: To associate floating IP

# Associate floating ip
 resource "openstack_networking_floatingip_v2" "floating_IP_MAAS" {
  pool = "public"
  port_id = "${openstack_networking_port_v2.MAAS_port_service.id}"

  depends_on = [openstack_compute_instance_v2.MAAS]

}

# Associate floating ip
 resource "openstack_networking_floatingip_v2" "floating_IP_Juju" {
  pool = "public"
  port_id = "${openstack_networking_port_v2.Juju_port_service.id}"

  depends_on = [openstack_compute_instance_v2.Juju]
}

# Associate floating ip
 resource "openstack_networking_floatingip_v2" "floating_IP_Node1" {
  pool = "public"
  port_id = "${openstack_networking_port_v2.Node1_port_service.id}"

  depends_on = [openstack_compute_instance_v2.Node1]

}


# Associate floating ip
 resource "openstack_networking_floatingip_v2" "floating_IP_Node2" {
  pool = "public"
  port_id = "${openstack_networking_port_v2.Node2_port_service.id}"

  depends_on = [openstack_compute_instance_v2.Node2]

}

# Associate floating ip
 resource "openstack_networking_floatingip_v2" "floating_IP_Node3" {
  pool = "public"
  port_id = "${openstack_networking_port_v2.Node3_port_service.id}"

  depends_on = [openstack_compute_instance_v2.Node3]

}