# Author : Bryan Haymoz
# Date   : 26.12.2021
# Version: 1.0
# Class  : T-3a
# Project: Semester Project 5 - Automated Deployement of VM's
# Purpose: Template for inventory of IP of subnet Service

Name=${hostname_Node1} IP=${IP_Node1_Service}

Name=${hostname_Node2} IP=${IP_Node2_Service}

Name=${hostname_Node3} IP=${IP_Node3_Service}

Name=${hostname_Juju} IP=${IP_Juju_Service}
