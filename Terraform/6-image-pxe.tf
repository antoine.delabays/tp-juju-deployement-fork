# Author : Bryan Haymoz
# Date   : 08.12.2021
# Version: 1.0
# Class  : T-3a
# Project: Semester Project 5 - Automated Deployement of VM's
# Purpose: To deploy pxe image

resource "openstack_images_image_v2" "pxe_image" {
  name             = "pxe_image"
  image_source_url = "${var.image_pxe_source}"
  container_format = "bare"
  disk_format      = "raw"
}