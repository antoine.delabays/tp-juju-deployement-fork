# Author : Bryan Haymoz
# Date   : 08.12.2021
# Version: 1.0
# Class  : T-3a
# Project: Semester Project 5 - Automated Deployement of VM's
# Purpose: To deploy network configuration

#Create service network
resource "openstack_networking_network_v2" "service_network" {
  name                  = "service_network"
  admin_state_up        = "true"
  port_security_enabled = "true"
}

#Create service subnet
resource "openstack_networking_subnet_v2" "service_subnet" {
  name            = "service_subnet"
  network_id      = "${openstack_networking_network_v2.service_network.id}"
  cidr            = "${var.service_network_cidr}"
  enable_dhcp     = "true"
  gateway_ip      = "${var.service_ip_gateway}"
  dns_nameservers =  ["${var.dns_server_1}", "${var.dns_server_2}"]
  ip_version      = 4
}

#Create pxe network
resource "openstack_networking_network_v2" "pxe_network" {
  name                  = "pxe_network"
  admin_state_up        = "true"
  port_security_enabled = "false"
}

#Create pxe subnet
resource "openstack_networking_subnet_v2" "pxe_subnet" {
  name                  = "pxe_subnet"
  network_id            = "${openstack_networking_network_v2.pxe_network.id}"
  enable_dhcp           = "false"
  no_gateway            = "true"
  cidr                  = "${var.pxe_network_cidr}"
  ip_version            = 4
}