# Author : Bryan Haymoz
# Date   : 20.12.2021
# Version: 1.0
# Class  : T-3a
# Project: Semester Project 5 - Automated Deployement of VM's
# Purpose: To create inventory of Terraform for Ansible

resource "local_file" "AnsibleInventory" {
content = templatefile("ansible_inventory.tpl", {
  hostname_MAAS        = "${openstack_compute_instance_v2.MAAS.name}",
  IP_MAAS     = "${openstack_networking_floatingip_v2.floating_IP_MAAS.address}",
  hostname_Juju        = "${openstack_compute_instance_v2.Juju.name}",
  IP_Juju     = "${openstack_networking_floatingip_v2.floating_IP_Juju.address}",
  hostname_Node1        = "${openstack_compute_instance_v2.Node1.name}",
  IP_Node1     = "${openstack_networking_floatingip_v2.floating_IP_Node1.address}",
  hostname_Node2        = "${openstack_compute_instance_v2.Node2.name}",
  IP_Node2     = "${openstack_networking_floatingip_v2.floating_IP_Node2.address}",
  hostname_Node3        = "${openstack_compute_instance_v2.Node3.name}",
  IP_Node3     = "${openstack_networking_floatingip_v2.floating_IP_Node3.address}",
  port_SSH = "${var.port_SSH}",
  user = "${var.ssh_user.MAAS}"
 }
 )
 filename = "../Ansible/dynamic_inventory"
}

resource "local_file" "MAAS" {
content = templatefile("MAAS_inventory.tpl", {
  IP_MAAS     = "${openstack_networking_floatingip_v2.floating_IP_MAAS.address}",
 }
 )
 filename = "../Ansible/IP_MAAS"
}

resource "local_file" "service_address" {
content = templatefile("service_address.tpl", {
  hostname_Node1        = "${openstack_compute_instance_v2.Node1.name}",
  IP_Node1_Service     = "${openstack_networking_port_v2.Node1_port_service.all_fixed_ips[0]}",
  hostname_Node2      = "${openstack_compute_instance_v2.Node2.name}",
  IP_Node2_Service     = "${openstack_networking_port_v2.Node2_port_service.all_fixed_ips[0]}",
  hostname_Node3        = "${openstack_compute_instance_v2.Node3.name}",
  IP_Node3_Service     = "${openstack_networking_port_v2.Node3_port_service.all_fixed_ips[0]}",
  hostname_Juju        = "${openstack_compute_instance_v2.Juju.name}",
  IP_Juju_Service     = "${openstack_networking_port_v2.Juju_port_service.all_fixed_ips[0]}",
 }
 )
 filename = "../Ansible/IP_Service"
}
