#!/bin/bash

# Author : Bryan Haymoz
# Date   : 04.01.2021
# Version: 1.0
# Class  : T-3a
# Project: Semester Project 5 - Automated Deployement of VM's
# Purpose: To reboot OpenStack Instance

# if empty parameter is passed, show message
if [ $# -lt 1 ] ; then
   echo "Parameters Need : Available parameters are {MAAS, Juju, Node1, Node2 or Node3}"
   exit 1
fi;

# if status paremeter is passed, show all vm status
if [ $1 = "status" ]; then
    openstack server list  -c Name -c Status
    exit 1    

# if all parameter is passed, reboot all vm
elif [ $1 = "all" ]; then
    status=$(echo -n "$(openstack server list -f value -c Name -c Status)" | tr '\n' ' ')
    
    # if one or more vm is already active, exit
    if [[ "$status" == *"ACTIVE"* ]]; then
        echo "can't reboot all machine because one or more is already active"
        exit 1
    else
        all=$(echo -n "$(openstack server list -f value -c Name)" | tr '\n' ' ')
        openstack server reboot $all
        exit 1
    fi;
else
    # if name of instance parameter is passed
    for vm in "$@" 
    do
        openstack server reboot $vm;
    done
fi;
