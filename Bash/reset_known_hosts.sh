#!/bin/bash

# Author : Bryan Haymoz
# Date   : 12.01.2022
# Version: 1.1
# Class  : T-3a
# Project: Semester Project 5 - Automated Deployement of VM's
# Purpose: To reset known_hosts

rm ~/.ssh/known_hosts
touch ~/.ssh/known_hosts
chmod 600 ~/.ssh/known_hosts
