#!/bin/sh

# Author : Bryan Haymoz
# Date   : 12.01.2022
# Version: 1.2
# Class  : T-3a
# Project: Semester Project 5 - Automated Deployement of VM's
# Purpose: To install software for automated deployement

# Install Terraform repository.
sudo apt-get update && sudo apt-get upgrade
sudo apt-get install -y gnupg software-properties-common curl
curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
sudo apt-get update && sudo apt-get install terraform

# Install Ansible repository.
sudo apt install ansible

# Install OpenStack CLI
sudo apt install python3-venv
python3 -m venv openstack_cli
sudo apt install python3-pip
sudo python3 -m pip install setuptools
sudo python3 -m pip install python-openstackclient

# Create SSH Keys
ssh-keygen -t rsa

# Install jq
snap install jq
