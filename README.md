# TP_Juju_DEPLOYEMENT

## Project : Automated Deployement of VM's

## Informations générales

* Etudiant : Bryan Haymoz
* Superviseur : Patrick Gaillet
* Co-superviseur : François Buntschu
* Dates : du 27.09.2021 au 09.02.2022

## Description

Ce repository contient les différents fichiers nécessaires pour la maquette d'automatisation du déploiement de l'infrastructure pour le TP K8s.

Le projet en rapport avec ce repository se trouve au lien suivant : https://gitlab.forge.hefr.ch/bryan.haymoz/ps5-tp_scripting

